"""dcu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView
# import modul service
from service import views
# import modul portfolio
from portfolio import viewPortfolio
# import modul team
from team import viewsteam
# import modul pricing
from pricing import viewspricing
from setting import viewssetting
# view image
from django.conf import settings 
from django.conf.urls.static import static

urlpatterns = [
    path('', views.home, name='home'),
    path('admin/', admin.site.urls),
    path('accounts/', include('users.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('empservice', views.emp),
    path('showservice',views.show),
    path('edit/<int:id>', views.edit),
    path('update/<int:id>', views.update),
    path('delete/<int:id>', views.destroy),
    path('showportfolio', viewPortfolio.showportfolio),
    path('saveportfolio', viewPortfolio.viewsave),
    path('deleteport/<int:id>', viewPortfolio.deleteport),
    path('editportfolio/<int:id>', viewPortfolio.editportfolio),
    path('updateportfolio/<int:id>', viewPortfolio.updateportfolio),
    path('teams', viewsteam.showteam),
    path('saveteams', viewsteam.saveteam),
    path('editeams/<int:id>', viewsteam.editteams),
    path('updateteams/<int:id>', viewsteam.updateteam),
    path('deleteteam/<int:id>', viewsteam.deleteteam),
    path('showpricing', viewspricing.showpricing),
    path('addpricing', viewspricing.viewPricing),
    path('editpricing/<int:id>', viewspricing.editPricing),
    path('updatepricing/<int:id>', viewspricing.updatePricing),
    path('deletepricing/<int:id>', viewspricing.deletePricing),
    path('setting', viewssetting.showsetting),
    path('addsetting', viewssetting.savesetting),
    path('deletesetting/<int:id>', viewssetting.deleteSetting)
]

if settings.DEBUG: 
        urlpatterns += static(settings.MEDIA_URL, 
                              document_root=settings.MEDIA_ROOT) 