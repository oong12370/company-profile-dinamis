from django.db import models

# Create your models here.
class Portfolio(models.Model):
    name = models.CharField(max_length=100)
    imageportfolio = models.ImageField(upload_to='images/')  
    ket = models.CharField(max_length=400)
    class Meta:
        db_table = "portfolio"