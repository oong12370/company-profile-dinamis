from django.shortcuts import render, redirect
from portfolio.forms import PortfolioForm  
from portfolio.models import Portfolio 
# Create your views here.

def showportfolio(request):
    portfolios = Portfolio.objects.all()
    return render(request,"view.html" ,{'portfolios':portfolios})

def viewsave(request):  
    if request.method == "POST":  
        form = PortfolioForm(request.POST,request.FILES)
        if form.is_valid():  
            try:  
                form.save()  
                return redirect("/showportfolio")  
            except:  
                pass  
    else:  
        form = PortfolioForm()  
    return render(request,'addport.html',{'form':form})

def editportfolio(request, id):  
    portfolio = Portfolio.objects.get(id=id)  
    return render(request,'editport.html', {'portfolio':portfolio})


def updateportfolio(request, id):  
    portfolio = Portfolio.objects.get(id=id)  
    form = PortfolioForm(request.POST, instance = portfolio)  
    if form.is_valid():  
        form.save()  
        return redirect("/showportfolio")  
    return render(request, 'view.html', {'portfolio': portfolio})

def deleteport(request, id):
    portfolio = Portfolio.objects.get(id=id)
    portfolio.delete()
    return redirect('/showportfolio')
