from django import forms
from setting.models import Setting
class SettingForm(forms.ModelForm):
    class Meta:
        model = Setting
        fields = "__all__"