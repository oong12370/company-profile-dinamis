from django.db import models

# Create your models here. 
class Setting(models.Model):
    location = models.CharField(max_length=100)
    phon = models.CharField(max_length=200)  
    mail = models.CharField(max_length=400)
    working = models.CharField(max_length=200)
    class Meta:
        db_table = "setting"