from django.shortcuts import render, redirect
from setting.forms import SettingForm  
from setting.models import Setting 
# Create your views here.

def showsetting(request):
    settings = Setting.objects.all()
    return render(request,"showsetting.html" ,{'settings':settings})

def savesetting(request):  
    if request.method == "POST":  
        form = SettingForm(request.POST)
        if form.is_valid():  
            try:  
                form.save()  
                return redirect("/setting")  
            except:  
                pass  
    else:  
        form = SettingForm()  
    return render(request,'addsetting.html',{'form':form})

def deleteSetting(request, id):
    setting = Setting.objects.get(id=id)
    setting.delete()
    return redirect('/setting')