from django.shortcuts import render, redirect
from team.forms import TeamForm  
from team.models import Team 
# Create your views here.

def showteam(request):
    teams = Team.objects.all()
    return render(request,"viewteam.html" ,{'teams':teams})

def saveteam(request):  
    if request.method == "POST":  
        form = TeamForm(request.POST,request.FILES)
        if form.is_valid():  
            try:  
                form.save()  
                return redirect("/teams")  
            except:  
                pass  
    else:  
        form = TeamForm()  
    return render(request,'addteam.html',{'form':form})

def editteams(request, id):  
    team = Team.objects.get(id=id)  
    return render(request,'edtteam.html', {'team':team})

def updateteam(request, id):  
    team = Team.objects.get(id=id)  
    form = TeamForm(request.POST, instance = team)  
    if form.is_valid():  
        form.save()  
        return redirect("/teams")  
    return render(request, 'view.html', {'team': team})

def deleteteam(request, id):
    team = Team.objects.get(id=id)
    team.delete()
    return redirect('/teams')
