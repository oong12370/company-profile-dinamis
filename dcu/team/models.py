from django.db import models

# Create your models here.
class Team(models.Model):
    name = models.CharField(max_length=100)
    imageteam = models.ImageField(upload_to='images/')  
    jabatan = models.CharField(max_length=400)
    email =models.CharField(max_length=200)
    class Meta:
        db_table = "team"