from django.shortcuts import render, redirect  
from service.forms import ServiceForm  
from service.models import Service  

from portfolio.forms import PortfolioForm  
from portfolio.models import Portfolio 

from team.forms import TeamForm  
from team.models import Team 

from pricing.forms import PricingForm  
from pricing.models import Pricing 


from setting.forms import SettingForm  
from setting.models import Setting 

# Create your views here.  
def emp(request):  
    if request.method == "POST":  
        form = ServiceForm(request.POST)
        if form.is_valid():  
            try:  
                form.save()  
                return redirect("/showservice")  
            except:  
                pass  
    else:  
        form = ServiceForm()  
    return render(request,'add.html',{'form':form})
def show(request):
    services = Service.objects.all()
    return render(request,"show.html",{'services':services})

def edit(request, id):  
    service = Service.objects.get(id=id)  
    return render(request,'edit.html', {'service':service})  

def update(request, id):  
    service = Service.objects.get(id=id)  
    form = ServiceForm(request.POST, instance = service)  
    if form.is_valid():  
        form.save()  
        return redirect("/showservice")  
    return render(request, 'show.html', {'service': service})

def destroy(request, id):  
    service = Service.objects.get(id=id)  
    service.delete()
    return redirect("/showservice")

def home(request):
    services = Service.objects.all()
    portfolio = Portfolio.objects.all()
    team = Team.objects.all()
    pricing = Pricing.objects.all()
    setting = Setting.objects.all()
    return render(request,"homepage.html",{'services':services,'portfolio':portfolio, 'team':team, 'pricing':pricing, 'setting':setting})

