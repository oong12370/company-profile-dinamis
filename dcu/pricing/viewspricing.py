from django.shortcuts import render, redirect
from pricing.forms import PricingForm  
from pricing.models import Pricing 

# Create your views here.

def showpricing(request):
    pricings = Pricing.objects.all()
    return render(request,"viewpricing.html" ,{'pricings':pricings})

def viewPricing(request):  
    if request.method == "POST":  
        form = PricingForm(request.POST)
        if form.is_valid():  
            try:  
                form.save()  
                return redirect("/showpricing")  
            except:  
                pass  
    else:
        form = PricingForm()  
    return render(request,'addpricing.html',{'form':form})

def editPricing(request,id):
    pricing = Pricing.objects.get(id=id)
    return render(request,'editpricing.html', {'pricing':pricing})

def updatePricing(request, id):  
    pricing = Pricing.objects.get(id=id)  
    form = PricingForm(request.POST, instance = pricing)  
    if form.is_valid():  
        form.save()  
        return redirect("/showpricing")  
    return render(request, 'viewpricing.html', {'pricing': pricing})


def deletePricing(request, id):
    pricing = Pricing.objects.get(id=id)
    pricing.delete()
    return redirect('/showpricing')