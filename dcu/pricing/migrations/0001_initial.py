# Generated by Django 3.0.3 on 2020-02-14 02:53

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Portfolio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('month', models.CharField(max_length=400)),
                ('disk', models.CharField(max_length=200)),
                ('email', models.CharField(max_length=200)),
                ('bandwidth', models.CharField(max_length=200)),
                ('subdomain', models.CharField(max_length=200)),
                ('domain', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'pricing',
            },
        ),
    ]
