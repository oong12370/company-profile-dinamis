from django import forms
from pricing.models import Pricing
class PricingForm(forms.ModelForm):
    class Meta:
        model = Pricing
        fields = "__all__"