from django.db import models

# Create your models here.
class Pricing(models.Model):
    name = models.CharField(max_length=100) 
    month = models.CharField(max_length=400)
    disk = models.CharField(max_length=200)
    email =models.CharField(max_length=200)
    bandwidth =models.CharField(max_length=200)
    subdomain =models.CharField(max_length=200)
    domain =models.CharField(max_length=200)
    class Meta:
        db_table = "pricing"